export const Colors = {
  titleColor: "#af5b5e",
  deleteBtnColor: "#af5b5e",
};

export const FontSizes = {
  primaryTextInput: "24px",
  secondaryTextInput: "24px",
};
