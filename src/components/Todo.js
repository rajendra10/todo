import React, { useState, useEffect } from "react";
import styled from "styled-components";
import TodoListing from "./TodoListing";
import Input from "./Input";
import { Colors } from "../constants/index";

const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  flex-direction: column;
`;
const Title = styled.h1`
  color: ${Colors.titleColor};
  font-size: 100px;
  font-weight: 300;
  text-align: center;
  color: rgba(175, 47, 47, 0.15);
`;
const Box = styled.div`
  width: 95vw;
  max-width: 600px;
  box-shadow: 0px 0px 10px 2px #d7d7d7;
`;
const TodoInputWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding: 20px 20px;
  height: 30px;
`;

const AllTodoToggler = styled.div`
  height: 30px;
  width: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
  transform: rotate(90deg);
  font-size: 24px;
  font-weight: 500;
  color: ${(props) => (props.allCompleted ? "black" : "gray")};
  cursor: pointer;
`;
const InputWrapper = styled.div`
  display: flex;
  flex: 1;
  position: relative;
`;
const initialTodos = [];
export default function Todo() {
  const [todos, setTodos] = useState([...initialTodos]);
  const [allCompleted, setAllCompleted] = useState(false);
  useEffect(() => {
    setAllCompleted(todos.every((todo) => todo.completed));
  }, [todos]);
  const addTodo = (task) => {
    if (!task) return;
    setTodos((prevTodos) => [
      { task: task, completed: false, id: Math.random() },
      ...prevTodos,
    ]);
  };
  const deleteTodo = (todoid) => {
    setTodos((prevTodos) => prevTodos.filter((todo) => todo.id !== todoid));
  };
  const toggleTodo = (todoid) => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) =>
        todo.id === todoid ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  const updateTodo = (task, id) => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) => (todo.id === id ? { ...todo, task } : todo))
    );
  };
  const toggleAllCompleted = () => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) => ({ ...todo, completed: !allCompleted }))
    );
  };

  const upsertTodo = (todoTask, id) => {
    if (id) {
      //update todo
      //if emtpy task delete the todo
      if (!todoTask) {
        deleteTodo(id);
      } else {
        //update todo
        updateTodo(todoTask, id);
      }
    } else {
      //add as new todo
      addTodo(todoTask);
    }
  };

  return (
    <Container>
      <Title>todos</Title>
      <Box>
        {/* todo input */}
        <TodoInputWrapper>
          {todos.length > 0 && (
            <AllTodoToggler
              onClick={toggleAllCompleted}
              allCompleted={allCompleted}
            >
              ❯
            </AllTodoToggler>
          )}
          <InputWrapper>
            <Input upsertTodo={upsertTodo} primary />
          </InputWrapper>
        </TodoInputWrapper>
        {/* todo listing */}
        <TodoListing
          todos={todos}
          toggleTodo={toggleTodo}
          deleteTodo={deleteTodo}
          upsertTodo={upsertTodo}
        />
      </Box>
    </Container>
  );
}
