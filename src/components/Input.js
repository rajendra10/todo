import React, { useState, useRef, useEffect } from "react";
import styled from "styled-components";
import { FontSizes } from "../constants";

const Container = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  flex: 1;
`;

const TextInput = styled.input`
  font-size: ${(props) =>
    props.primary ? FontSizes.primaryTextInput : FontSizes.secondaryTextInput};
  width: 100%;
  height: 100%;
  outline: none;
  border: ${(props) => (props.border ? "solid 1.5px gray" : "none")};
  padding: 0px 10px;
  box-sizing: border-box;
  &::placeholder {
    opacity: 0.3;
  }
`;
export default function Input({ todo, upsertTodo, disableEditMode, primary }) {
  const [todoValue, setTodoValue] = useState(todo ? todo.task : "");
  const inputRef = useRef(null);

  useEffect(() => {
    todo && inputRef.current.focus();
  }, [todo]);

  const handleChange = (e) => {
    setTodoValue(e.target.value);
  };
  const handleBlur = () => {
    disableEditMode && handleKeyPress({ key: "Enter" });
  };
  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      upsertTodo(todoValue, todo && todo.id);
      setTodoValue("");
      disableEditMode && disableEditMode();
    } else if (e.key === "Escape") {
      setTodoValue(todo ? todo.task : "");
      disableEditMode && disableEditMode();
    }
  };

  return (
    <Container>
      <TextInput
        border={!!todo}
        primary={primary}
        ref={inputRef}
        placeholder={todo ? "" : "Whats on your mind? "}
        value={todoValue}
        onChange={handleChange}
        onBlur={handleBlur}
        onKeyUp={handleKeyPress}
      />
    </Container>
  );
}
