import React, { useState } from "react";
import styled from "styled-components";
import Input from "./Input";
import { Colors, FontSizes } from "../constants";

const Container = styled.div``;
const TodoWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  position: relative;
  border-top: solid 1px #d7d7d7;
`;
const Toggler = styled.div`
  height: 40px;
  width: 40px;
  padding: 15px 10px;
  margin-right: 10px;
  cursor: pointer;
`;

const TodoTextWrapper = styled.div`
  display: flex;
  flex: 1;
  position: relative;
`;

const DeleteBtn = styled.div`
  position: absolute;
  display: none;
  right: 0;
  color: ${Colors.deleteBtnColor};
  height: 30px;
  width: 30px;
  dispay: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  font-weight: 500;
  cursor: pointer;
`;
const TodoText = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  padding-right: 35px;
  margin: 0px;
  font-size: ${FontSizes.secondaryTextInput};
  text-decoration: ${(props) => (props.completed ? "line-through" : "none")};
  opacity: ${(props) => (props.completed ? 0.5 : 1)};
  transition: opacity 0.3s linear;
  line-break: anywhere;
  &:hover .deleteBtn {
    display: block;
  }
`;
const CheckUncheckIcon = styled.img`
  height: inherit;
  width: inherit;
`;

export default function TodoListing({
  todos,
  toggleTodo,
  deleteTodo,
  upsertTodo,
}) {
  const [editModeTaskIndex, setEditModeTaskIndex] = useState(false);
  return (
    <Container>
      {todos.map((todo, i) => {
        return (
          <TodoWrapper key={`${todo.id}_${todo.task}`}>
            <Toggler onClick={() => toggleTodo(todo.id)}>
              <CheckUncheckIcon
                src={todo.completed ? "/checked.svg" : "/unchecked.svg"}
              />
            </Toggler>
            <TodoTextWrapper>
              {editModeTaskIndex === i ? (
                <Input
                  disableEditMode={() => setEditModeTaskIndex(-1)}
                  todo={todo}
                  upsertTodo={upsertTodo}
                />
              ) : (
                <TodoText
                  completed={todo.completed}
                  onDoubleClick={() => setEditModeTaskIndex(i)}
                >
                  {todo.task}
                  <DeleteBtn
                    className="deleteBtn"
                    onClick={() => deleteTodo(todo.id)}
                  >
                    X
                  </DeleteBtn>
                </TodoText>
              )}
            </TodoTextWrapper>
          </TodoWrapper>
        );
      })}
    </Container>
  );
}
